// global vars for data & selected_id
let data;
let selected_id;
// set event handler for selected id
d3.selectAll('#sel_sub_id').on('change', buildDashboard);
init();

function init() {
  //read samples.json with D3
  d3.json('samples.json').then(payload => {
    data = payload;
    //Set Select Options
    let subj_ids = payload.names;
    let select_el = d3.select('#sel_sub_id');
    subj_ids.forEach((value, index) => {
      opt = select_el.append('option');
      opt.text(value).attr('value', function () { return value; });
      if(index == 0){
        opt.attr('selected', true);
      }
    });
  }).then(payload => {
    buildDashboard();
  });
};

// Function build plot for bar chart, bubble chart and gauge chart
function buildDashboard() {
  topTenBarChart();
  demographics();
  bubbleChart();
  washingGauge();
}

function getOtuLabels(otu_ids) {
  let top_ten_otu_labels = [];
  for (let i = 0; i < otu_ids.length; i++) {
    top_ten_otu_labels.push(`OTU ${otu_ids[i]}`);
  }
  return top_ten_otu_labels;
};

function getBacteriaName(name) {
  let bactNames = [];
  for (let i = 0; i < name.length; i++) {
    let stringName = name[i].toString();
    let splitValue = stringName.split(";");
    if (splitValue.length > 1) {
      bactNames.push(splitValue[splitValue.length - 1]);
    } else {
      bactNames.push(splitValue[0]);
    }
  }
  return bactNames;
};

// Create a horizontal bar chart with a dropdown menu to display the top 10 OTUs found in that individual
function topTenBarChart() {
  selected_id = d3.select('#sel_sub_id').node().value;
  let filtertopTen = data.samples.filter(value => value.id == selected_id);
  let otu_ids = filtertopTen.map(d => d.otu_ids);
  if(otu_ids.length == 0){
    console.log("no data found for selected subject id");
  }else{
    top_ten_ids = otu_ids[0].slice(0, 10);
    yvals = getOtuLabels(top_ten_ids);
    let xvals = filtertopTen.map(d => d.sample_values)[0].slice(0, 10);
    let labels = filtertopTen.map(d => d.otu_labels)[0];
    let names = getBacteriaName(labels).slice(0, 10);
    // Traces for horizontal bar chart
    let traceBar = {
      x: xvals,
      y: yvals,
      text: names,
      type: 'bar',
      orientation: 'h'
    };
    let layout = {
      yaxis: {
        autorange: 'reversed'
      }
    };
    let plotBardata = [traceBar];
    let config = { responsive: true }
    Plotly.newPlot('bar', plotBardata, layout, config);
  }
};

// Function demographics extract metadata text for id, ethnicity, gender, age, location, bbtype and wfreq
function demographics() {
  selected_id = d3.select('#sel_sub_id').node().value;
  let filt_demog = data.metadata.filter(value => value.id == selected_id);
  let demographics = d3.select('.list-group-flush');
  demographics.html('');
  demographics.append('li').attr('class','list-group-item').text(`id: ${filt_demog[0].id}`);
  demographics.append('li').attr('class','list-group-item').text(`ethnicity: ${filt_demog[0].ethnicity}`);
  demographics.append('li').attr('class','list-group-item').text(`gender: ${filt_demog[0].gender}`);
  demographics.append('li').attr('class','list-group-item').text(`age: ${filt_demog[0].age}`);
  demographics.append('li').attr('class','list-group-item').text(`location: ${filt_demog[0].location}`);
  demographics.append('li').attr('class','list-group-item').text(`bbtype: ${filt_demog[0].bbtype}`);
  demographics.append('li').attr('class','list-group-item').text(`wfreq: ${filt_demog[0].wfreq}`);
}

// Create a bubble chart that displays each sample
function bubbleChart() {
  selected_id = d3.select('#sel_sub_id').node().value;
  let filterBubble = data.samples.filter(value => value.id == selected_id);
  let xvalues = filterBubble.map(d => d.otu_ids)[0];
  let yValue = filterBubble.map(d => d.sample_values)[0];
  let labels = filterBubble.map(d => d.otu_labels);
  labels = getBacteriaName(labels[0]);
  let traceBubble = {
    x: xvalues,
    y: yValue,
    mode: 'markers',
    marker: {
      size: yValue,
      color: ['#67c2a6', '#67c2bd', '#67adc2', '#6796c2', '#6781c2', '#676dc2', '#7b67c2', '#8e67c2', '#9c67c2'],
    },
    text: labels
  };
  let plotBubbledata = [traceBubble];
  var layout = {
    showlegend: false,
    xaxis: {title: 'OTU ID'},
    margin: {
      t: 0, b: 0
    } 
  };
  let config = {responsive: true}  
  Plotly.newPlot('bubble', plotBubbledata, layout, config);
};

// Create a gauge chart to display washing frequency between 0-9  
function washingGauge(selected_id) {
  selected_id = d3.select('#sel_sub_id').node().value;
  let filterGauge = data.metadata.filter(value => value.id == selected_id);
  let weeklyFrequency = filterGauge[0].wfreq;
  let plotGaugedata = [
    {
      domain: { x: [0, 1], y: [0, 1] },
      title: {
        text: 'Belly Button Washing Scrubs/Week'
      },
      type: 'indicator',
      mode: 'gauge',
      gauge: {
        axis: {
          range: [0, 9],
          tickvals: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          ticks: 'outside'
        },
        steps: [
          { range: [0, 1], color: '#a83232'},
          { range: [1, 2], color: '#a84e32'},
          { range: [2, 3], color: '#a86932'},
          { range: [3, 4], color: '#a88e32'},
          { range: [4, 5], color: '#a89e32'},
          { range: [5, 6], color: '#9ca832'},
          { range: [6, 7], color: '#8ca832'},
          { range: [7, 8], color: '#7ba832'},
          { range: [8, 9], color: '#61a832'}
        ],
        threshold: {
          line: {color: 'black', width: 3},
          thickness: 3,
          value: weeklyFrequency
        }
      }
    }
  ];

  let layout = { 
    width: 500, 
    height: 500, 
    margin: { 
      t: 0, b: 0
    } 
  };

  let config = {responsive: true}    

  Plotly.newPlot('gauge', plotGaugedata, layout, config);
};




